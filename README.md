# vifm-ueberzug
Ueberzug support for vifm. This is a fork of cirala's vifmimg repository.

## Dependencies
- vifm
- ueberzug
- ffmpegthumbnailer (https://github.com/dirkvdb/ffmpegthumbnailer)
  - For media previews
- epub-thumbnailer (https://github.com/marianosimone/epub-thumbnailer)
  - For epub previews
- pdftoppm (poppler)
  - For pdf previews (https://poppler.freedesktop.org)
- ddjvu
  - For djvu previews (https://github.com/traycold/djvulibre)
- ffmpeg
  - For audio previews (https://ffmpeg.org)
- fontpreview
  - For font previews (https://github.com/sdushantha/fontpreview)

## Usage
Install vifm-ueberzug along with its dependencies.
To use it, simply run vifmrun. Note that vifmrun must run *instead* of vifm. If this is too inconvenient, I recommend you create a symlink or alias.

## Configuration
You need to edit your vifmrc configuration slightly. Taken from the original repository, this is an example of how it should be used.

```
    fileviewer *.pdf
        \ vifmimg pdf %px %py %pw %ph %c
        \ %pc
        \ vifmimg clear

    fileviewer *.djvu
        \ vifmimg djvu %px %py %pw %ph %c
        \ %pc
        \ vifmimg clear

    fileviewer *.epub
        \ vifmimg epub %px %py %pw %ph %c
        \ %pc
        \ vifmimg clear

    fileviewer <video/*>
        \ vifmimg video %px %py %pw %ph %c
        \ %pc
        \ vifmimg clear

    fileviewer <image/*>
        \ vifmimg draw %px %py %pw %ph %c
        \ %pc
        \ vifmimg clear

    fileviewer <audio/*>
        \ vifmimg audio %px %py %pw %ph %c
        \ %pc
        \ vifmimg clear

    fileviewer <font/*>
        \ vifmimg font %px %py %pw %ph %c
        \ %pc
        \ vifmimg clear

```

If this seems to difficult, you can use [my vifm configuration](https://codeberg.org/speedie/dotfiles) instead.

## Why a fork?
This fork exists, because the original version seems to be unmaintained. I want to actively make improvements to it!
In addition, the original version had sudo as a dependency in the Makefile which is bad practice. I wanted to write a Gentoo ebuild and something like this should **not** require sudo. 

## License
The original author used the GPLv3 license (good choice!) for their project so this fork is licensed under that same license!

## Gentoo ebuild
You can install the vifm-ueberzug package on Gentoo by adding [my overlay](https://codeberg.org/speedie/speedie-overlay).
