SHELL = /bin/sh
NAME = vifm-ueberzug
FILE1 = vifmimg
FILE2 = vifmrun
VER = 0.1

help:
	@echo "make install     Install ${NAME}."
	@echo "make uninstall   Uninstall ${NAME}."

install:
	cp ${FILE1} ${FILE2} ${DESTDIR}${PREFIX}/bin
	chmod +x "${DESTDIR}${PREFIX}/bin/${FILE1}" "${DESTDIR}${PREFIX}/bin/${FILE2}"

uninstall:
	rm -f "${DESTDIR}${PREFIX}/bin/${FILE1}"
	rm -f "${DESTDIR}${PREFIX}/bin/${FILE2}"

dist:
	mkdir -p ${NAME}-${VER}
	cp ${FILE1} ${FILE2} Makefile LICENSE README.md ${NAME}-${VER}
	tar -cf ${NAME}-${VER}.tar ${NAME}-${VER}
	gzip ${NAME}-${VER}.tar
	rm -rf ${NAME}-${VER}.tar ${NAME}-${VER}
